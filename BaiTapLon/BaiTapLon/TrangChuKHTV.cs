﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class TrangChuKHTV : Form
    {
        public TrangChuKHTV()
        {
            InitializeComponent();
        }



        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
        DataTable dt = new DataTable();
        public void ketnoi()
        {
            //ImageList imageList1 = new ImageList();
            lblTen.Text = Program.getTenKH();

            cnn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand("select * from sanpham", cnn);

            adapter.Fill(dt);
            int img = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                imageList1.Images.Add(Image.FromFile(dt.Rows[i][5].ToString()));
                var viewItem = new ListViewItem(dt.Rows[i][1].ToString() + "\nGiá:" + dt.Rows[i][2].ToString());
                viewItem.ImageIndex = img;
                lvSanPham.Items.Add(viewItem);
                img++;
            }
        }
        
        private void TrangChuKHTV_Load(object sender, EventArgs e)
        {
            lvSanPham.Items.Clear();
            ketnoi();
            
        }
        private void lvSanPham_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            
            int var2 = 0;
            if (lvSanPham.SelectedItems.Count > 0)
            {
                for (int lcount = 0; lcount <= lvSanPham.Items.Count - 1; lcount++)
                {
                    if (lvSanPham.Items[lcount].Selected == true)
                    {
                        var2 = lcount;
                        break;
                    }
                }
            }
            string maKH = Program.getMS();

            SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
            cnn.Open();
            
            string check = "select count(*) from giohang where msgh = '" + maKH + "'and mssp = '"+dt.Rows[var2][0].ToString()+"'";
            SqlCommand cmd = new SqlCommand(check, cnn);
            int count = (int)cmd.ExecuteScalar();
            if (count != 0)
            {
                MessageBox.Show("Sản phẩm đã tồn tại trong giỏ hàng. Nếu bạn muốn mua thêm, vui lòng chỉnh sửa số lượng trong giỏ hàng của bạn","Thông báo");
            }
            else
            {
                string sql = "insert into Giohang(msgh,mssp,tensp,soluong,tongtien) values(@msgh,@mssp,@tensp,@soluong,@giatien)";
                cmd = new SqlCommand(sql, cnn);
                cmd.Parameters.AddWithValue("@msgh", maKH);
                cmd.Parameters.AddWithValue("@mssp", dt.Rows[var2][0].ToString());
                cmd.Parameters.AddWithValue("@tensp", dt.Rows[var2][1].ToString());
                cmd.Parameters.AddWithValue("@soluong", 1);
                cmd.Parameters.AddWithValue("@giatien", Convert.ToDouble(dt.Rows[var2][2]));
                MessageBox.Show("Thêm vào giỏ hàng thành công");
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
            
        }

        private void giỏHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Program.getGioHang();
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Program.getDangNhap();
        }
    }
}
