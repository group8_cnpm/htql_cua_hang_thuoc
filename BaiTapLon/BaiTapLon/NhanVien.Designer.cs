﻿
namespace BaiTapLon
{
    partial class NhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridNhanVien = new System.Windows.Forms.DataGridView();
            this.lbDanhSachNhanVien = new System.Windows.Forms.Label();
            this.btThemNhanVien = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridNhanVien)).BeginInit();
            this.SuspendLayout();
            // 
            // gridNhanVien
            // 
            this.gridNhanVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridNhanVien.Location = new System.Drawing.Point(28, 63);
            this.gridNhanVien.Name = "gridNhanVien";
            this.gridNhanVien.RowHeadersWidth = 51;
            this.gridNhanVien.RowTemplate.Height = 24;
            this.gridNhanVien.Size = new System.Drawing.Size(946, 547);
            this.gridNhanVien.TabIndex = 0;
            this.gridNhanVien.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // lbDanhSachNhanVien
            // 
            this.lbDanhSachNhanVien.AutoSize = true;
            this.lbDanhSachNhanVien.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDanhSachNhanVien.Location = new System.Drawing.Point(351, 9);
            this.lbDanhSachNhanVien.Name = "lbDanhSachNhanVien";
            this.lbDanhSachNhanVien.Size = new System.Drawing.Size(314, 39);
            this.lbDanhSachNhanVien.TabIndex = 1;
            this.lbDanhSachNhanVien.Text = "Danh Sách Nhân Viên";
            // 
            // btThemNhanVien
            // 
            this.btThemNhanVien.Location = new System.Drawing.Point(82, 644);
            this.btThemNhanVien.Name = "btThemNhanVien";
            this.btThemNhanVien.Size = new System.Drawing.Size(158, 23);
            this.btThemNhanVien.TabIndex = 2;
            this.btThemNhanVien.Text = "Thêm Nhân Viên";
            this.btThemNhanVien.UseVisualStyleBackColor = true;
            this.btThemNhanVien.Click += new System.EventHandler(this.btThemNhanVien_Click);
            // 
            // NhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.btThemNhanVien);
            this.Controls.Add(this.lbDanhSachNhanVien);
            this.Controls.Add(this.gridNhanVien);
            this.Name = "NhanVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NhanVien";
            this.Load += new System.EventHandler(this.NhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridNhanVien)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridNhanVien;
        private System.Windows.Forms.Label lbDanhSachNhanVien;
        private System.Windows.Forms.Button btThemNhanVien;
    }
}