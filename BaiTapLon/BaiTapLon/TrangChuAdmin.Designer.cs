﻿
namespace BaiTapLon
{
    partial class TrangChuAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrangChuAdmin));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuNhanVien = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSanPham = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuThongKe = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêLươngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêTàiChínhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêĐơnHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thayĐổiMậtKhẩuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.lbTenDangNhap = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btXoaNhanVien = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.gridNhanVien = new System.Windows.Forms.DataGridView();
            this.txtTimKiem = new System.Windows.Forms.TextBox();
            this.lblTuKhoa = new System.Windows.Forms.Label();
            this.btnTimKiem = new System.Windows.Forms.Button();
            this.btnHienThi = new System.Windows.Forms.Button();
            this.lblDanhSach = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridNhanVien)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNhanVien,
            this.mnuSanPham,
            this.mnuThongKe,
            this.thayĐổiMậtKhẩuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1006, 41);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuNhanVien
            // 
            this.mnuNhanVien.BackColor = System.Drawing.Color.Transparent;
            this.mnuNhanVien.Font = new System.Drawing.Font("Times New Roman", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuNhanVien.ForeColor = System.Drawing.Color.Yellow;
            this.mnuNhanVien.Name = "mnuNhanVien";
            this.mnuNhanVien.Size = new System.Drawing.Size(152, 37);
            this.mnuNhanVien.Text = "Nhân viên";
            this.mnuNhanVien.Click += new System.EventHandler(this.mnuNhanVien_Click);
            // 
            // mnuSanPham
            // 
            this.mnuSanPham.BackColor = System.Drawing.Color.Transparent;
            this.mnuSanPham.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuSanPham.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mnuSanPham.Name = "mnuSanPham";
            this.mnuSanPham.Size = new System.Drawing.Size(138, 37);
            this.mnuSanPham.Text = "Sản phẩm";
            this.mnuSanPham.Click += new System.EventHandler(this.mnuSanPham_Click);
            // 
            // mnuThongKe
            // 
            this.mnuThongKe.BackColor = System.Drawing.Color.Transparent;
            this.mnuThongKe.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thốngKêLươngToolStripMenuItem,
            this.thốngKêTàiChínhToolStripMenuItem,
            this.thốngKêĐơnHàngToolStripMenuItem});
            this.mnuThongKe.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuThongKe.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mnuThongKe.Name = "mnuThongKe";
            this.mnuThongKe.Size = new System.Drawing.Size(133, 37);
            this.mnuThongKe.Text = "Thống kê";
            // 
            // thốngKêLươngToolStripMenuItem
            // 
            this.thốngKêLươngToolStripMenuItem.Name = "thốngKêLươngToolStripMenuItem";
            this.thốngKêLươngToolStripMenuItem.Size = new System.Drawing.Size(319, 38);
            this.thốngKêLươngToolStripMenuItem.Text = "Thống kê lương";
            // 
            // thốngKêTàiChínhToolStripMenuItem
            // 
            this.thốngKêTàiChínhToolStripMenuItem.Name = "thốngKêTàiChínhToolStripMenuItem";
            this.thốngKêTàiChínhToolStripMenuItem.Size = new System.Drawing.Size(319, 38);
            this.thốngKêTàiChínhToolStripMenuItem.Text = "Thống kê tài chính ";
            // 
            // thốngKêĐơnHàngToolStripMenuItem
            // 
            this.thốngKêĐơnHàngToolStripMenuItem.Name = "thốngKêĐơnHàngToolStripMenuItem";
            this.thốngKêĐơnHàngToolStripMenuItem.Size = new System.Drawing.Size(319, 38);
            this.thốngKêĐơnHàngToolStripMenuItem.Text = "Thống kê đơn hàng";
            // 
            // thayĐổiMậtKhẩuToolStripMenuItem
            // 
            this.thayĐổiMậtKhẩuToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.thayĐổiMậtKhẩuToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thayĐổiMậtKhẩuToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.thayĐổiMậtKhẩuToolStripMenuItem.Name = "thayĐổiMậtKhẩuToolStripMenuItem";
            this.thayĐổiMậtKhẩuToolStripMenuItem.Size = new System.Drawing.Size(235, 37);
            this.thayĐổiMậtKhẩuToolStripMenuItem.Text = "Thay đổi mật khẩu";
            this.thayĐổiMậtKhẩuToolStripMenuItem.Click += new System.EventHandler(this.thayĐổiMậtKhẩuToolStripMenuItem_Click);
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.BackColor = System.Drawing.SystemColors.Control;
            this.btnDangXuat.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangXuat.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDangXuat.Location = new System.Drawing.Point(742, 658);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(162, 51);
            this.btnDangXuat.TabIndex = 1;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.UseVisualStyleBackColor = false;
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
            // 
            // lbTenDangNhap
            // 
            this.lbTenDangNhap.AutoSize = true;
            this.lbTenDangNhap.BackColor = System.Drawing.Color.Transparent;
            this.lbTenDangNhap.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbTenDangNhap.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenDangNhap.ForeColor = System.Drawing.Color.Transparent;
            this.lbTenDangNhap.Location = new System.Drawing.Point(720, 9);
            this.lbTenDangNhap.Name = "lbTenDangNhap";
            this.lbTenDangNhap.Size = new System.Drawing.Size(97, 26);
            this.lbTenDangNhap.TabIndex = 3;
            this.lbTenDangNhap.Text = "Chưa rõ";
            this.lbTenDangNhap.Click += new System.EventHandler(this.lbTenDangNhap_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(80, 658);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 51);
            this.button1.TabIndex = 5;
            this.button1.Text = "Thêm nhân viên";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btXoaNhanVien
            // 
            this.btXoaNhanVien.BackColor = System.Drawing.SystemColors.Control;
            this.btXoaNhanVien.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btXoaNhanVien.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btXoaNhanVien.Location = new System.Drawing.Point(527, 658);
            this.btXoaNhanVien.Name = "btXoaNhanVien";
            this.btXoaNhanVien.Size = new System.Drawing.Size(188, 51);
            this.btXoaNhanVien.TabIndex = 6;
            this.btXoaNhanVien.Text = "Xoá nhân viên";
            this.btXoaNhanVien.UseVisualStyleBackColor = false;
            this.btXoaNhanVien.Click += new System.EventHandler(this.btXoaNhanVien_Click);
            // 
            // btnSua
            // 
            this.btnSua.BackColor = System.Drawing.SystemColors.Control;
            this.btnSua.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSua.Location = new System.Drawing.Point(322, 658);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(176, 51);
            this.btnSua.TabIndex = 7;
            this.btnSua.Text = "Sửa nhân viên";
            this.btnSua.UseVisualStyleBackColor = false;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click_1);
            // 
            // gridNhanVien
            // 
            this.gridNhanVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridNhanVien.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridNhanVien.Location = new System.Drawing.Point(0, 198);
            this.gridNhanVien.Name = "gridNhanVien";
            this.gridNhanVien.ReadOnly = true;
            this.gridNhanVien.RowHeadersWidth = 51;
            this.gridNhanVien.RowTemplate.Height = 24;
            this.gridNhanVien.Size = new System.Drawing.Size(1006, 454);
            this.gridNhanVien.TabIndex = 9;
            this.gridNhanVien.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridNhanVien_CellContentClick);
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTimKiem.Location = new System.Drawing.Point(181, 147);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(357, 27);
            this.txtTimKiem.TabIndex = 10;
            // 
            // lblTuKhoa
            // 
            this.lblTuKhoa.AutoSize = true;
            this.lblTuKhoa.BackColor = System.Drawing.Color.Transparent;
            this.lblTuKhoa.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTuKhoa.ForeColor = System.Drawing.Color.White;
            this.lblTuKhoa.Location = new System.Drawing.Point(-6, 141);
            this.lblTuKhoa.Name = "lblTuKhoa";
            this.lblTuKhoa.Size = new System.Drawing.Size(181, 33);
            this.lblTuKhoa.TabIndex = 11;
            this.lblTuKhoa.Text = "Nhập từ khóa: ";
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.BackColor = System.Drawing.SystemColors.Control;
            this.btnTimKiem.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnTimKiem.Location = new System.Drawing.Point(553, 142);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(117, 36);
            this.btnTimKiem.TabIndex = 12;
            this.btnTimKiem.Text = "Tìm";
            this.btnTimKiem.UseVisualStyleBackColor = false;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // btnHienThi
            // 
            this.btnHienThi.BackColor = System.Drawing.SystemColors.Control;
            this.btnHienThi.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHienThi.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnHienThi.Location = new System.Drawing.Point(830, 141);
            this.btnHienThi.Name = "btnHienThi";
            this.btnHienThi.Size = new System.Drawing.Size(164, 37);
            this.btnHienThi.TabIndex = 12;
            this.btnHienThi.Text = "Hiển thị tất cả";
            this.btnHienThi.UseVisualStyleBackColor = false;
            this.btnHienThi.Click += new System.EventHandler(this.btnHienThi_Click);
            // 
            // lblDanhSach
            // 
            this.lblDanhSach.AutoSize = true;
            this.lblDanhSach.BackColor = System.Drawing.Color.Transparent;
            this.lblDanhSach.Font = new System.Drawing.Font("Times New Roman", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhSach.ForeColor = System.Drawing.Color.White;
            this.lblDanhSach.Location = new System.Drawing.Point(313, 55);
            this.lblDanhSach.Name = "lblDanhSach";
            this.lblDanhSach.Size = new System.Drawing.Size(377, 49);
            this.lblDanhSach.TabIndex = 13;
            this.lblDanhSach.Text = "Danh sách nhân viên";
            // 
            // TrangChuAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.lblDanhSach);
            this.Controls.Add(this.btnHienThi);
            this.Controls.Add(this.btnTimKiem);
            this.Controls.Add(this.lblTuKhoa);
            this.Controls.Add(this.txtTimKiem);
            this.Controls.Add(this.gridNhanVien);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btXoaNhanVien);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbTenDangNhap);
            this.Controls.Add(this.btnDangXuat);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TrangChuAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrangChu";
            this.Load += new System.EventHandler(this.TrangChuAdmin_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridNhanVien)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuNhanVien;
        private System.Windows.Forms.ToolStripMenuItem mnuSanPham;
        private System.Windows.Forms.ToolStripMenuItem mnuThongKe;
        private System.Windows.Forms.ToolStripMenuItem thốngKêLươngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêTàiChínhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêĐơnHàngToolStripMenuItem;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.Label lbTenDangNhap;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btXoaNhanVien;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.ToolStripMenuItem thayĐổiMậtKhẩuToolStripMenuItem;
        private System.Windows.Forms.DataGridView gridNhanVien;
        private System.Windows.Forms.TextBox txtTimKiem;
        private System.Windows.Forms.Label lblTuKhoa;
        private System.Windows.Forms.Button btnTimKiem;
        private System.Windows.Forms.Button btnHienThi;
        private System.Windows.Forms.Label lblDanhSach;
    }
}