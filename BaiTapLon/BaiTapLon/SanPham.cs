﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class SanPham : Form
    {
        public SanPham()
        {
            InitializeComponent();
        }

        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        public void ketnoi()
        {
            cnn.Open();
            string sql = "select * from SanPham";
            SqlCommand com = new SqlCommand(sql, cnn);
            com.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(com);

            DataTable dt = new DataTable();
            da.Fill(dt);
            cnn.Close();
            gridSanPham.DataSource = dt;
        }


        private void btnCapNhap_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getThongTinSanPham();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getThemSanPham();
        
        }

        private void SanPham_Load(object sender, EventArgs e)
        {
            ketnoi();
            lbTenDangNhap.Text = Program.getTenDN();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getTrangChuAdmin();
        }


        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getDangNhap();
            Program.setNull();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int i;
            i = gridSanPham.CurrentRow.Index;
            string ms = gridSanPham.Rows[i].Cells[0].Value.ToString();

            DialogResult dlr = MessageBox.Show("Bạn có chắc chắn muốn xoá nhân viên có mã sô" + ms,
            "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
                cnn.Open();
                string sql = "delete from SanPham where mssp LIKE '" + ms + "' ";
                SqlCommand com = new SqlCommand(sql, cnn);
                com.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(com);
                com.ExecuteNonQuery();
                MessageBox.Show("Xoá thành công sản phẩm " + ms, "Thông báo");
                Program.getSanPham();
            }
            else
                MessageBox.Show("Thao tác đã được huỷ", "Thông báo");
            cnn.Close();
        }
        public string getMaSoSP()
        {
            int i;
            i = gridSanPham.CurrentRow.Index;
            string ms = gridSanPham.Rows[i].Cells[0].Value.ToString();
            return ms;
        }

        private void mnuNhanVien_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getTrangChuAdmin();
        }

        private void mnuSanPham_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bạn đang ở trang quản lý sản phẩm","Thông báo");
        }


        private void gridSanPham_SelectionChanged(object sender, EventArgs e)
        {
            int i;
            
            if(gridSanPham.CurrentCell != null)
            {
                i = gridSanPham.CurrentCell.RowIndex;
                string row = gridSanPham.Rows[i].Cells[0].Value.ToString();
                string n = null;
                string sql1 = "SELECT ANH FROM SANPHAM WHERE MSSP LIKE'" + row + "'";
                cnn.Open();
                SqlCommand cmd1 = new SqlCommand(sql1, cnn);
                try
                {
                    n = (string)cmd1.ExecuteScalar();
                    pcBox.Image = new Bitmap(n);
                }
                catch (Exception)
                {

                }
            }
            cnn.Close();
        }

        public void loadGridByKeyWord()
        {
            cnn.Open();
            string sql = "select * from SanPham where tensp like N'%" + this.txtTimKiem.Text + "%'";
            SqlCommand com = new SqlCommand(sql, cnn);
            com.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(com);

            DataTable dt = new DataTable();
            da.Fill(dt);
            cnn.Close();
            gridSanPham.DataSource = dt;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            loadGridByKeyWord();
        }

        private void btnHienThi_Click(object sender, EventArgs e)
        {
            ketnoi();
        }

        private void thayĐổiMậtKhẩuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getDoiMatKhau();
        }
    }
}
