﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class DangNhap : Form
    {
        public DangNhap()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btDangNhap_Click(object sender, EventArgs e)
        {
            SqlConnection taikhoan = new SqlConnection(SqlConnect.ConnectionString);
            try
            {
                taikhoan.Open();
                string msnd = txtDangNhap.Text;
                string matKhau = txtMatKhau.Text;
                string sql = "select * from NguoiDung where msnd = '" +msnd+"'and matkhau =  '"+ matKhau+"'";
                SqlCommand cmd = new SqlCommand(sql, taikhoan);
                cmd.CommandType = CommandType.Text;
                SqlDataReader data = cmd.ExecuteReader();
                SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
                if (data.Read())
                {
                    try {
                        this.Hide();
                        cnn.Open();
                        string sql1 = "SELECT hoten FROM NguoiDung where msnd ='" + msnd + "'";
                        SqlCommand cmd1 = new SqlCommand(sql1, cnn);
                        cmd.CommandType = CommandType.Text;
                        string n = (string)cmd1.ExecuteScalar();
                        sql1 = "SELECT tentaikhoan FROM NguoiDung where msnd ='" + msnd + "'";
                        cmd1 = new SqlCommand(sql1, cnn);
                        cmd.CommandType = CommandType.Text;
                        string m = (string)cmd1.ExecuteScalar();
                        if (m.Equals("admin") || m.Equals("ADMIN"))
                        {
                            Program.getTrangChuAdmin(n);

                        }
                        else
                        {
                            Program.getTrangChuKHTV();
                        }
                    }
                    catch(Exception)
                    {
                        MessageBox.Show("Khong the lay ten dang nhap");
                        Program.getTrangChuAdmin();
                    }
                    
                    
                }
                else
                {
                    MessageBox.Show("Sai mật khẩu hoặc tên đăng nhập","THÔNG BÁO");
                }
                taikhoan.Close();
                cnn.Close();
            }
            catch(Exception)
            {
                MessageBox.Show("Lỗi kết nối....");
            }
        }
        public string getMS()
        {
            return txtDangNhap.Text.ToString();
        }
        private void llbDangKy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            Program.getDangKy();
        }

        private void llbQuenMatKhau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("Chức năng đang trong quá trình phát triển","Thông báo");
        }

        public void setNull()
        {
            this.txtDangNhap.Text = String.Empty;
            this.txtMatKhau.Text = String.Empty;
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void lbMatKhau_Click(object sender, EventArgs e)
        {

        }

        public string getTenKH()
        {
            SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
            cnn.Open();
            string msnd = txtDangNhap.Text;
            string sql1 = "SELECT hoten FROM NguoiDung where msnd ='" + msnd + "'";
            SqlCommand cmd = new SqlCommand(sql1, cnn);
            cmd.CommandType = CommandType.Text;
            string n = (string)cmd.ExecuteScalar();
            cnn.Close();
            return n;
        }

        public string getSdt()
        {
            SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
            cnn.Open();
            string msnd = txtDangNhap.Text;
            string sql1 = "SELECT sdt FROM NguoiDung where msnd ='" + msnd + "'";
            SqlCommand cmd = new SqlCommand(sql1, cnn);
            cmd.CommandType = CommandType.Text;
            string n = (string)cmd.ExecuteScalar();
            cnn.Close();
            return n;
        }
    }
}
