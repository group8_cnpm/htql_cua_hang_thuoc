﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class ThanhToan : Form
    {
        public ThanhToan()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Đặt hàng thành công!!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void lbTrangChu_Click(object sender, EventArgs e)
        {
            
        }

        private void tàiKhoảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void trangChủToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TrangChuKHTV khtv = new TrangChuKHTV();
            this.Hide();
            khtv.ShowDialog();
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DangNhap dn = new DangNhap();
            this.Hide();
            dn.ShowDialog();
        }

        private void giỏHàngToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GioHang gh = new GioHang();
            this.Hide();
            gh.ShowDialog();
        }

        private void ThanhToan_Load(object sender, EventArgs e)
        {
            lblMaSo.Text = Program.getMS();
            lblHoVaTen.Text = Program.getTenKH();
            lblTongTien.Text = Convert.ToString(Program.getTongTien());
            lblSdt.Text = Program.getSdt();  
        }

        private void btnTroVe_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getGioHang();
        }

        private void chkTrucTiep_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTrucTiep.Checked)
            {
                chkOnline.Enabled = false;
            }
            else
            {
                chkOnline.Enabled = true;
            }    
        }

        private void chkOnline_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOnline.Checked)
            {
                chkTrucTiep.Enabled = false;
            }
            else
            {
                chkTrucTiep.Enabled = true;
            }    
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            int tongtien = Program.getTongTien();

            if (this.txtDiachi.Text == String.Empty)
            {
                MessageBox.Show("Bạn chưa nhập địa chỉ giao hàng","Thông báo");
            }    
            else if (tongtien <= 0)
            {
                MessageBox.Show("Chưa có sản phẩm trong giỏ hàng", "Thông báo");
            }
            else if ((chkOnline.Checked == false) && (chkTrucTiep.Checked) == false)
            {
                MessageBox.Show("Chưa chọn hình thức thanh toán", "Thông báo");
            }    
            else
            {
                if (dtpNgayLap.Value >= DateTime.Today)
                {
                    if (chkTrucTiep.Checked)
                    {
                        MessageBox.Show("Đặt hàng thành công", "Thông báo");
                        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
                        cnn.Open();
                        string msnd = Program.getMS();
                        string sql1 = "delete FROM giohang where msgh ='" + msnd + "'";
                        SqlCommand cmd = new SqlCommand(sql1, cnn);
                        cmd.ExecuteNonQuery();
                        cnn.Close();

                        this.Hide();
                        Program.getTrangChuKHTV();
                    }
                    else
                    {
                        MessageBox.Show("Đang trong quá trình phát triển chức năng thanh toán online", "Thông báo");
                        //this.Hide();
                        //Program.getThanhToanOnline();
                    }
                }
                else
                {
                    MessageBox.Show("Ngày thanh toán phải lớn hơn hoặc bằng ngày hiện tại: " + DateTime.Today.ToString(), "Thông báo");

                }
            }
              
        }
    }
}
