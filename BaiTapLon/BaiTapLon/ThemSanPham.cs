﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Common;
using System.IO;

namespace BaiTapLon
{
    public partial class ThemSanPham : Form
    {
        public ThemSanPham()
        {
            InitializeComponent();
        }

        private void ThemSanPham_Load(object sender, EventArgs e)
        {
           
        }

        
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getSanPham();
            setNull();
        }

        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        private void setNull()
        {
            txtAnh.Text = null;
            txtDonVi.Text = null;
            txtGiaTien.Text = null;
            txtTenSanPham.Text = null;
            txtSoLuong.Text = null;
        }
        public bool checkNumber(string n)
        {
            foreach (char c in n)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (this.txtTenSanPham.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập thông tin tên sản phẩm", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (this.txtGiaTien.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập thông tin giá tiền", "Lỗi", MessageBoxButtons.OKCancel);
            } 
            else if (this.txtDonVi.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập thông tin đơn vị", "Lỗi", MessageBoxButtons.OKCancel);
            }  
            else if (this.txtSoLuong.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập thông tin số lượng", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (!checkNumber(this.txtGiaTien.Text) || !checkNumber(this.txtSoLuong.Text))
            {
                MessageBox.Show("Giá tiền hoặc số lượng phải là kiểu dữ liệu số", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else
            {
                cnn.Open();
                 
                string sql = "insert into SanPham(tensp,giatien,donvi,soluong,anh) values(@tensp,@giatien,@donvi,@soluong,@anh)";
                SqlCommand cmd = new SqlCommand(sql, cnn);
                cmd.Parameters.AddWithValue("@tensp", this.txtTenSanPham.Text);
                cmd.Parameters.AddWithValue("@giatien", this.txtGiaTien.Text);
                cmd.Parameters.AddWithValue("@donvi", this.txtDonVi.Text);
                cmd.Parameters.AddWithValue("@soluong", this.txtSoLuong.Text);
                cmd.Parameters.AddWithValue("@anh", this.txtAnh.Text);
                cmd.ExecuteNonQuery();
                string sql1 = "SELECT MAX(MSSP) FROM SANPHAM";
                SqlCommand cmd1 = new SqlCommand(sql1, cnn);

                string n = (string)cmd1.ExecuteScalar();
                cnn.Close();

                string re = "Thêm sản phẩm với mã số: " + n.ToString() + " thành công";

                MessageBox.Show(re, "Thông báo", MessageBoxButtons.OKCancel);
                cnn.Close();
            }
            setNull();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnPicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = ("|*.jpg");
            if(open.ShowDialog() == DialogResult.OK)
            {
                txtAnh.Text = open.FileName;
            }
        }
    }
}
