﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class GioHang : Form
    {
        public GioHang()
        {
            InitializeComponent();
        }
        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        public void ketnoi()
        {
            
            cnn.Open();
            //string sql = "select * from GIOHANG where msgh ='" + Program.getMS()+"'group by msgh";
            string sql = "select MSSP,TENSP,SOLUONG,TONGTIEN from GIOHANG where msgh = '" + Program.getMS()+"'";
            SqlCommand com = new SqlCommand(sql, cnn);
            com.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cnn.Close();
            dtGioHang.DataSource = dt;
            lbTongGia.Text = tongTien().ToString();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            cnn.Open();
            int i;
            i = dtGioHang.CurrentRow.Index;
            string sql = "delete from GIOHANG Where  STT='" + dtGioHang.Rows[i].Cells[0].Value.ToString() + "'";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.ExecuteNonQuery();
            cnn.Close();
            ketnoi();
        }


        private void trangChủToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getTrangChuKHTV();
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            Program.getDangNhap();
        }

        private void GioHang_Load_1(object sender, EventArgs e)
        {
            lblTen.Text = Program.getTenKH();
            lbTongGia.Text = tongTien().ToString();
            ketnoi();
        }

        private void dtGioHang_SelectionChanged(object sender, EventArgs e)
        {
            int i;
            cnn.Open();
            if (dtGioHang.CurrentCell != null)
            {
                i = dtGioHang.CurrentCell.RowIndex;
                string row = dtGioHang.Rows[i].Cells[0].Value.ToString();
                string n = null;
                string sql1 = "SELECT ANH FROM SANPHAM WHERE MSSP LIKE'" + row + "'";
                SqlCommand cmd1 = new SqlCommand(sql1, cnn);
                try
                {
                    n = (string)cmd1.ExecuteScalar();
                    pcGioHang.Image = new Bitmap(n);
                    txtSoLuong.Text = dtGioHang.Rows[i].Cells[2].Value.ToString() ;
                    lbSanPham.Text = dtGioHang.Rows[i].Cells[1].Value.ToString();
                }
                catch (Exception)
                {

                }
            }
            cnn.Close();
        }
        public int tongTien()
        {
            try
            {
                cnn.Open();
                String sql = "select sum(tongtien) from giohang where msgh = '" + Program.getMS() + "'";
                SqlCommand cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                object re = cmd.ExecuteScalar();
                cnn.Close();
                return Convert.ToInt32(re);
            }catch(Exception)
            {
                return 0;
            }
            
        }
        private void btCapNhap_Click(object sender, EventArgs e)
        {
            if (txtSoLuong.Text != null && Convert.ToInt32(txtSoLuong.Text)>0)
            {
                cnn.Open();
                int i = dtGioHang.CurrentCell.RowIndex;
                string row = dtGioHang.Rows[i].Cells[0].Value.ToString();
                string sql = "update giohang set soluong = '" + Convert.ToInt32(txtSoLuong.Text) + "' where mssp = '" + row + "'";
                SqlCommand cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                sql = "select giatien from sanpham where mssp = '" + row + "'";
                cmd = new SqlCommand(sql, cnn);
                int n = (int)cmd.ExecuteScalar();
                sql = "update giohang set tongtien = '" + n * Convert.ToInt32(txtSoLuong.Text) + "' where mssp = '" + row + "'";
                cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                sql = "select sum(tongtien) from giohang where msgh = '"+Program.getMS()+"'";
                cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                cnn.Close();
                ketnoi();
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra số lượng");
            }
            
        }

        private void btXoa_Click(object sender, EventArgs e)
        {
            cnn.Open();
            int i = dtGioHang.CurrentCell.RowIndex;
            string row = dtGioHang.Rows[i].Cells[0].Value.ToString();
            string sql = "delete from giohang where mssp = '" + row + "'";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.ExecuteNonQuery();
            cnn.Close();
            ketnoi();
        }

        private void btDatHang_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getThanhToan();
        }
    }
}
