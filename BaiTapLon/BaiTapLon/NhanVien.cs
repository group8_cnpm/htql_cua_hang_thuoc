﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class NhanVien : Form
    {
        public NhanVien()
        {
            InitializeComponent();
        }
        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        public void ketnoi()
        {
            cnn.Open();
            string sql = "select * from NguoiDung where tentaikhoan = N'Nhân Viên' ";
            SqlCommand com = new SqlCommand(sql, cnn);
            com.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(com);

            DataTable dt = new DataTable();
            da.Fill(dt);
            cnn.Close();
            gridNhanVien.DataSource = dt;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void NhanVien_Load(object sender, EventArgs e)
        {
            ketnoi();
        }

        private void btThemNhanVien_Click(object sender, EventArgs e)
        {
            Program.getThemNhanVien();
        }
    }
}
