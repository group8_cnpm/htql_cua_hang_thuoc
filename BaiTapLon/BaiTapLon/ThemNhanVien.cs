﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class ThemNhanVien : Form
    {
        public ThemNhanVien()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        private void ThemNhanVien_Load(object sender, EventArgs e)
        {
            this.txtTenTK.Text = "Nhân viên";
        }
        private void setNull()
        {
            txtDiaChi.Text = null;
            txtHoTen.Text = null;
            txtLuong.Text = null;
            txtMatKhau.Text = null;
            txtSDT.Text = null;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (this.txtHoTen.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập họ tên nhân viên", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (this.txtSDT.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập số điện thoại nhân viên", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (this.txtLuong.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập lương của nhân viên", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (this.txtMatKhau.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập mật khẩu cho tài khoản nhân viên", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else
            {
                cnn.Open();
                string checkSDT = "select count(*) from nguoidung where sdt = '" + txtSDT.Text + "'";
                SqlCommand cmd = new SqlCommand(checkSDT, cnn);
                int count = (int)cmd.ExecuteScalar();
                if (count != 0)
                {
                    MessageBox.Show("Số điện thoại đã tồn tại", "Lỗi", MessageBoxButtons.OKCancel);
                }
                else
                {
                    string sql = "insert into NguoiDung(hoten,sdt,diachi,luong,tentaikhoan,matkhau) values(@hoten,@sdt,@diachi,@luong,@tentaikhoan,@matkhau)";
                    cmd = new SqlCommand(sql, cnn);
                    cmd.Parameters.AddWithValue("@hoten", this.txtHoTen.Text);
                    cmd.Parameters.AddWithValue("@sdt", this.txtSDT.Text);
                    cmd.Parameters.AddWithValue("@luong", this.txtLuong.Text);
                    cmd.Parameters.AddWithValue("@tentaikhoan", this.txtTenTK.Text);
                    cmd.Parameters.AddWithValue("@diachi", this.txtDiaChi.Text);
                    cmd.Parameters.AddWithValue("@matkhau", this.txtMatKhau.Text);
                    cmd.ExecuteNonQuery();
                    cnn.Close();
                    cnn.Open();
                    string sql1 = "SELECT MAX(MSND) FROM NguoiDung";
                    SqlCommand cmd1 = new SqlCommand(sql1, cnn);

                    string n = (string)cmd1.ExecuteScalar();
                   

                    string re = "Thêm Nhân viên với mã số: " + n.ToString() + " thành công";

                    MessageBox.Show(re, "Thông báo", MessageBoxButtons.OKCancel);
                }    
            }

            setNull();
            cnn.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getTrangChuAdmin();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
