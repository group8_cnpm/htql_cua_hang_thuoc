﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class TrangChuAdmin : Form
    {
        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
        public TrangChuAdmin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void picTrangChu_Click(object sender, EventArgs e)
        {

        }

        SanPham p = new SanPham();
        private void mnuSanPham_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getSanPham();
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getDangNhap();
            Program.setNull();
        }

        private void lbTenTaiKhoa_Click(object sender, EventArgs e)
        {

        }

        private void mnuNhanVien_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bạn đang ở trang chủ quản lý nhân viên","Thông báo");
        }
        public void ketnoi()
        {
            
            cnn.Open();
            string sql = "select * from NguoiDung where tentaikhoan = N'Nhân Viên' ";
            SqlCommand com = new SqlCommand(sql, cnn);
            com.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(com);

            DataTable dt = new DataTable();
            da.Fill(dt);
            cnn.Close();
            gridNhanVien.DataSource = dt;
        }
        private void TrangChuAdmin_Load(object sender, EventArgs e)
        {
            ketnoi();
        }
        public void setTenDangNhap(String ten)
        {
            lbTenDangNhap.Text = ten;
        }


        private void lbTenDangNhap_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getThemNhanVien();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void btXoaNhanVien_Click(object sender, EventArgs e)
        {
            int i;
            i = gridNhanVien.CurrentRow.Index;
            string ms = gridNhanVien.Rows[i].Cells[0].Value.ToString();
            
            DialogResult dlr = MessageBox.Show("Bạn có chắc chắn muốn xoá nhân viên có mã sô"+ms,
            "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
                cnn.Open();
                string sql = "delete from NguoiDung where msnd LIKE '" + ms + "' ";
                SqlCommand com = new SqlCommand(sql, cnn);
                com.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(com);
                com.ExecuteNonQuery();
                cnn.Close();
                MessageBox.Show("Xoá thành công nhân viên "+ms,"Thông báo");
            }
            else
                MessageBox.Show("Thao tác đã được huỷ","Thông báo");

            ketnoi();
        }

        private void thayĐổiMậtKhẩuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.getDoiMatKhau();
        }

        public string getMSNhanVien()
        {
            SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);
            int i;
            i = gridNhanVien.CurrentRow.Index;
            string ms = gridNhanVien.Rows[i].Cells[0].Value.ToString();
            return ms;
        }

        private void btnSua_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Program.getThongTinNhanVien();
        }

        public void loadGridByKeyWord()
        {
            cnn.Open();
            string sql = "select * from NguoiDung where tentaikhoan = N'Nhân Viên' and hoten like N'%"+this.txtTimKiem.Text+"%' ";
            SqlCommand com = new SqlCommand(sql, cnn);
            com.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(com);

            DataTable dt = new DataTable();
            da.Fill(dt);
            cnn.Close();
            gridNhanVien.DataSource = dt;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            loadGridByKeyWord();
        }

        private void btnHienThi_Click(object sender, EventArgs e)
        {
            ketnoi();
        }

        private void gridNhanVien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
