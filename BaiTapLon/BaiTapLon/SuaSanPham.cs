﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class SuaSanPham : Form
    {
        public SuaSanPham()
        {
            InitializeComponent();
        }

        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        private void SuaSanPham_Load(object sender, EventArgs e)
        {
            string ms = Program.getMSSP();
            cnn.Open();
            string sql1 = "select tensp from SanPham where mssp LIKE '" + ms + "'";
            SqlCommand cmd1 = new SqlCommand(sql1, cnn);
            this.txtTenSanPham.Text = (string)cmd1.ExecuteScalar();

            string sql2 = "select giatien from SanPham where mssp LIKE '" + ms + "'";
            SqlCommand cmd2 = new SqlCommand(sql2, cnn);
            this.txtGiaTien.Text = Convert.ToString((int)cmd2.ExecuteScalar());

            string sql3 = "select donvi from SanPham where mssp LIKE '" + ms + "'";
            SqlCommand cmd3 = new SqlCommand(sql3, cnn);
            this.txtDonVi.Text = (string)cmd3.ExecuteScalar();

            string sql4 = "select soluong from SanPham where mssp LIKE '" + ms + "'";
            SqlCommand cmd4 = new SqlCommand(sql4, cnn);
            this.txtSoLuong.Text = Convert.ToString((int)cmd4.ExecuteScalar());

            cnn.Close();
        }

        private void btnCapNhap_Click(object sender, EventArgs e)
        {
            if (!(Program.checkNum(this.txtGiaTien.Text)) || !(Program.checkNum(this.txtSoLuong.Text)))
            {
                MessageBox.Show("Giá tiền hoặc số lượng phải là kiểu dữ liệu số", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else
            {
                string ms = Program.getMSSP();
                cnn.Open();
                string sql = "Update SanPham set tensp = @tensp, giatien = @giatien, donvi = @donvi, soluong = @soluong where mssp LIKE '" + ms + "'";
                SqlCommand cmd = new SqlCommand(sql, cnn);
                cmd.Parameters.AddWithValue("@tensp", this.txtTenSanPham.Text);
                cmd.Parameters.AddWithValue("@giatien", this.txtGiaTien.Text);
                cmd.Parameters.AddWithValue("@donvi", this.txtDonVi.Text);
                cmd.Parameters.AddWithValue("@soluong", this.txtSoLuong.Text);

                cmd.ExecuteNonQuery();
                cnn.Close();

                this.Hide();
                Program.getSanPham();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getSanPham();
        }
    }
}
