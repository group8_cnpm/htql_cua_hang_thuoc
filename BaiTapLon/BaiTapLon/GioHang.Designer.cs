﻿
namespace BaiTapLon
{
    partial class GioHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GioHang));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.trangChủToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tàiKhoảnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đăngXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thayĐổiThôngTinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xemĐơnHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giỏHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dtGioHang = new System.Windows.Forms.DataGridView();
            this.lbGioHang = new System.Windows.Forms.Label();
            this.pcGioHang = new System.Windows.Forms.PictureBox();
            this.txtSoLuong = new System.Windows.Forms.TextBox();
            this.lbSoLuong = new System.Windows.Forms.Label();
            this.btCapNhap = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbTongGia = new System.Windows.Forms.Label();
            this.btXoa = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbSanPham = new System.Windows.Forms.Label();
            this.lblTen = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btDatHang = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGioHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcGioHang)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trangChủToolStripMenuItem,
            this.tàiKhoảnToolStripMenuItem,
            this.xemĐơnHàngToolStripMenuItem,
            this.giỏHàngToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1006, 35);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // trangChủToolStripMenuItem
            // 
            this.trangChủToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trangChủToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.trangChủToolStripMenuItem.Name = "trangChủToolStripMenuItem";
            this.trangChủToolStripMenuItem.Size = new System.Drawing.Size(122, 31);
            this.trangChủToolStripMenuItem.Text = "Trang chủ";
            this.trangChủToolStripMenuItem.Click += new System.EventHandler(this.trangChủToolStripMenuItem_Click);
            // 
            // tàiKhoảnToolStripMenuItem
            // 
            this.tàiKhoảnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.đăngXuấtToolStripMenuItem,
            this.thayĐổiThôngTinToolStripMenuItem});
            this.tàiKhoảnToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tàiKhoảnToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tàiKhoảnToolStripMenuItem.Name = "tàiKhoảnToolStripMenuItem";
            this.tàiKhoảnToolStripMenuItem.Size = new System.Drawing.Size(122, 31);
            this.tàiKhoảnToolStripMenuItem.Text = "Tài khoản";
            // 
            // đăngXuấtToolStripMenuItem
            // 
            this.đăngXuấtToolStripMenuItem.Name = "đăngXuấtToolStripMenuItem";
            this.đăngXuấtToolStripMenuItem.Size = new System.Drawing.Size(274, 32);
            this.đăngXuấtToolStripMenuItem.Text = "Đăng xuất";
            this.đăngXuấtToolStripMenuItem.Click += new System.EventHandler(this.đăngXuấtToolStripMenuItem_Click);
            // 
            // thayĐổiThôngTinToolStripMenuItem
            // 
            this.thayĐổiThôngTinToolStripMenuItem.Name = "thayĐổiThôngTinToolStripMenuItem";
            this.thayĐổiThôngTinToolStripMenuItem.Size = new System.Drawing.Size(274, 32);
            this.thayĐổiThôngTinToolStripMenuItem.Text = "Thay đổi thông tin";
            // 
            // xemĐơnHàngToolStripMenuItem
            // 
            this.xemĐơnHàngToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xemĐơnHàngToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.xemĐơnHàngToolStripMenuItem.Name = "xemĐơnHàngToolStripMenuItem";
            this.xemĐơnHàngToolStripMenuItem.Size = new System.Drawing.Size(167, 31);
            this.xemĐơnHàngToolStripMenuItem.Text = "Xem đơn hàng";
            // 
            // giỏHàngToolStripMenuItem
            // 
            this.giỏHàngToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.giỏHàngToolStripMenuItem.ForeColor = System.Drawing.Color.Yellow;
            this.giỏHàngToolStripMenuItem.Name = "giỏHàngToolStripMenuItem";
            this.giỏHàngToolStripMenuItem.Size = new System.Drawing.Size(113, 31);
            this.giỏHàngToolStripMenuItem.Text = "Giỏ hàng";
            // 
            // dtGioHang
            // 
            this.dtGioHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGioHang.Location = new System.Drawing.Point(12, 99);
            this.dtGioHang.Name = "dtGioHang";
            this.dtGioHang.ReadOnly = true;
            this.dtGioHang.RowHeadersWidth = 51;
            this.dtGioHang.RowTemplate.Height = 24;
            this.dtGioHang.Size = new System.Drawing.Size(606, 598);
            this.dtGioHang.TabIndex = 1;
            this.dtGioHang.SelectionChanged += new System.EventHandler(this.dtGioHang_SelectionChanged);
            // 
            // lbGioHang
            // 
            this.lbGioHang.AutoSize = true;
            this.lbGioHang.BackColor = System.Drawing.Color.Transparent;
            this.lbGioHang.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGioHang.ForeColor = System.Drawing.Color.White;
            this.lbGioHang.Location = new System.Drawing.Point(67, 49);
            this.lbGioHang.Name = "lbGioHang";
            this.lbGioHang.Size = new System.Drawing.Size(395, 37);
            this.lbGioHang.TabIndex = 2;
            this.lbGioHang.Text = "Các sản phẩm trong giỏ hàng";
            // 
            // pcGioHang
            // 
            this.pcGioHang.Location = new System.Drawing.Point(652, 152);
            this.pcGioHang.Name = "pcGioHang";
            this.pcGioHang.Size = new System.Drawing.Size(311, 227);
            this.pcGioHang.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcGioHang.TabIndex = 3;
            this.pcGioHang.TabStop = false;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Location = new System.Drawing.Point(828, 400);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(135, 22);
            this.txtSoLuong.TabIndex = 4;
            // 
            // lbSoLuong
            // 
            this.lbSoLuong.AutoSize = true;
            this.lbSoLuong.BackColor = System.Drawing.Color.Transparent;
            this.lbSoLuong.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoLuong.ForeColor = System.Drawing.Color.White;
            this.lbSoLuong.Location = new System.Drawing.Point(703, 400);
            this.lbSoLuong.Name = "lbSoLuong";
            this.lbSoLuong.Size = new System.Drawing.Size(103, 27);
            this.lbSoLuong.TabIndex = 5;
            this.lbSoLuong.Text = "Số lượng:";
            // 
            // btCapNhap
            // 
            this.btCapNhap.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCapNhap.Location = new System.Drawing.Point(828, 443);
            this.btCapNhap.Name = "btCapNhap";
            this.btCapNhap.Size = new System.Drawing.Size(135, 38);
            this.btCapNhap.TabIndex = 6;
            this.btCapNhap.Text = "Cập nhập";
            this.btCapNhap.UseVisualStyleBackColor = true;
            this.btCapNhap.Click += new System.EventHandler(this.btCapNhap_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(677, 604);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 33);
            this.label1.TabIndex = 7;
            this.label1.Text = "Tổng tiền:";
            // 
            // lbTongGia
            // 
            this.lbTongGia.AutoSize = true;
            this.lbTongGia.BackColor = System.Drawing.Color.Transparent;
            this.lbTongGia.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongGia.ForeColor = System.Drawing.Color.Red;
            this.lbTongGia.Location = new System.Drawing.Point(812, 604);
            this.lbTongGia.Name = "lbTongGia";
            this.lbTongGia.Size = new System.Drawing.Size(29, 33);
            this.lbTongGia.TabIndex = 8;
            this.lbTongGia.Text = "0";
            // 
            // btXoa
            // 
            this.btXoa.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btXoa.Location = new System.Drawing.Point(640, 443);
            this.btXoa.Name = "btXoa";
            this.btXoa.Size = new System.Drawing.Size(182, 38);
            this.btXoa.TabIndex = 9;
            this.btXoa.Text = "Xoá sản phẩm";
            this.btXoa.UseVisualStyleBackColor = true;
            this.btXoa.Click += new System.EventHandler(this.btXoa_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(647, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 27);
            this.label2.TabIndex = 10;
            this.label2.Text = "Sản phẩm:";
            // 
            // lbSanPham
            // 
            this.lbSanPham.AutoSize = true;
            this.lbSanPham.BackColor = System.Drawing.Color.Transparent;
            this.lbSanPham.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSanPham.ForeColor = System.Drawing.Color.Red;
            this.lbSanPham.Location = new System.Drawing.Point(765, 99);
            this.lbSanPham.Name = "lbSanPham";
            this.lbSanPham.Size = new System.Drawing.Size(90, 27);
            this.lbSanPham.TabIndex = 11;
            this.lbSanPham.Text = "Chưa rõ";
            // 
            // lblTen
            // 
            this.lblTen.AutoSize = true;
            this.lblTen.BackColor = System.Drawing.Color.Transparent;
            this.lblTen.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.ForeColor = System.Drawing.Color.White;
            this.lblTen.Location = new System.Drawing.Point(804, 0);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(90, 27);
            this.lblTen.TabIndex = 2;
            this.lblTen.Text = "Chưa rõ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(693, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 27);
            this.label3.TabIndex = 12;
            this.label3.Text = "Xin chào,";
            // 
            // btDatHang
            // 
            this.btDatHang.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDatHang.Location = new System.Drawing.Point(818, 659);
            this.btDatHang.Name = "btDatHang";
            this.btDatHang.Size = new System.Drawing.Size(135, 38);
            this.btDatHang.TabIndex = 13;
            this.btDatHang.Text = "Đặt hàng";
            this.btDatHang.UseVisualStyleBackColor = true;
            this.btDatHang.Click += new System.EventHandler(this.btDatHang_Click);
            // 
            // GioHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.btDatHang);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbSanPham);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btXoa);
            this.Controls.Add(this.lbTongGia);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btCapNhap);
            this.Controls.Add(this.lbSoLuong);
            this.Controls.Add(this.txtSoLuong);
            this.Controls.Add(this.pcGioHang);
            this.Controls.Add(this.lblTen);
            this.Controls.Add(this.lbGioHang);
            this.Controls.Add(this.dtGioHang);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GioHang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GioHang";
            this.Load += new System.EventHandler(this.GioHang_Load_1);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGioHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcGioHang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem trangChủToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tàiKhoảnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đăngXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thayĐổiThôngTinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xemĐơnHàngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem giỏHàngToolStripMenuItem;
        private System.Windows.Forms.DataGridView dtGioHang;
        private System.Windows.Forms.Label lbGioHang;
        private System.Windows.Forms.PictureBox pcGioHang;
        private System.Windows.Forms.TextBox txtSoLuong;
        private System.Windows.Forms.Label lbSoLuong;
        private System.Windows.Forms.Button btCapNhap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbTongGia;
        private System.Windows.Forms.Button btXoa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbSanPham;
        private System.Windows.Forms.Label lblTen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btDatHang;
    }
}