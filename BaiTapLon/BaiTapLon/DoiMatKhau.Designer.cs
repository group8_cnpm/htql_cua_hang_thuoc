﻿
namespace BaiTapLon
{
    partial class DoiMatKhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoiMatKhau));
            this.bt_Thoat = new System.Windows.Forms.Button();
            this.bt_DoiMK = new System.Windows.Forms.Button();
            this.txtb_XacNhanMK = new System.Windows.Forms.TextBox();
            this.txtb_MKMoi = new System.Windows.Forms.TextBox();
            this.txtb_MKCu = new System.Windows.Forms.TextBox();
            this.txtb_TenDangNhap = new System.Windows.Forms.TextBox();
            this.lb_XacNhanMK = new System.Windows.Forms.Label();
            this.lb_MKCu = new System.Windows.Forms.Label();
            this.lb_MKMoi = new System.Windows.Forms.Label();
            this.lb_TenDangNhap = new System.Windows.Forms.Label();
            this.lb_DMK = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_Thoat
            // 
            this.bt_Thoat.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_Thoat.Location = new System.Drawing.Point(590, 527);
            this.bt_Thoat.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bt_Thoat.Name = "bt_Thoat";
            this.bt_Thoat.Size = new System.Drawing.Size(114, 38);
            this.bt_Thoat.TabIndex = 14;
            this.bt_Thoat.Text = "Thoát";
            this.bt_Thoat.UseVisualStyleBackColor = true;
            this.bt_Thoat.Click += new System.EventHandler(this.bt_Thoat_Click);
            // 
            // bt_DoiMK
            // 
            this.bt_DoiMK.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_DoiMK.Location = new System.Drawing.Point(346, 527);
            this.bt_DoiMK.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bt_DoiMK.Name = "bt_DoiMK";
            this.bt_DoiMK.Size = new System.Drawing.Size(200, 38);
            this.bt_DoiMK.TabIndex = 15;
            this.bt_DoiMK.Text = "Đổi mật khẩu";
            this.bt_DoiMK.UseVisualStyleBackColor = true;
            this.bt_DoiMK.Click += new System.EventHandler(this.bt_DoiMK_Click);
            // 
            // txtb_XacNhanMK
            // 
            this.txtb_XacNhanMK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtb_XacNhanMK.Location = new System.Drawing.Point(379, 456);
            this.txtb_XacNhanMK.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtb_XacNhanMK.Name = "txtb_XacNhanMK";
            this.txtb_XacNhanMK.Size = new System.Drawing.Size(325, 30);
            this.txtb_XacNhanMK.TabIndex = 10;
            // 
            // txtb_MKMoi
            // 
            this.txtb_MKMoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtb_MKMoi.Location = new System.Drawing.Point(379, 385);
            this.txtb_MKMoi.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtb_MKMoi.Name = "txtb_MKMoi";
            this.txtb_MKMoi.Size = new System.Drawing.Size(322, 30);
            this.txtb_MKMoi.TabIndex = 11;
            // 
            // txtb_MKCu
            // 
            this.txtb_MKCu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtb_MKCu.Location = new System.Drawing.Point(379, 304);
            this.txtb_MKCu.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtb_MKCu.Name = "txtb_MKCu";
            this.txtb_MKCu.Size = new System.Drawing.Size(322, 30);
            this.txtb_MKCu.TabIndex = 12;
            // 
            // txtb_TenDangNhap
            // 
            this.txtb_TenDangNhap.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtb_TenDangNhap.Location = new System.Drawing.Point(379, 217);
            this.txtb_TenDangNhap.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtb_TenDangNhap.Name = "txtb_TenDangNhap";
            this.txtb_TenDangNhap.Size = new System.Drawing.Size(322, 30);
            this.txtb_TenDangNhap.TabIndex = 13;
            // 
            // lb_XacNhanMK
            // 
            this.lb_XacNhanMK.AutoSize = true;
            this.lb_XacNhanMK.BackColor = System.Drawing.Color.Transparent;
            this.lb_XacNhanMK.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_XacNhanMK.ForeColor = System.Drawing.Color.White;
            this.lb_XacNhanMK.Location = new System.Drawing.Point(133, 453);
            this.lb_XacNhanMK.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb_XacNhanMK.Name = "lb_XacNhanMK";
            this.lb_XacNhanMK.Size = new System.Drawing.Size(236, 33);
            this.lb_XacNhanMK.TabIndex = 8;
            this.lb_XacNhanMK.Text = "Xác nhận mật khẩu:";
            // 
            // lb_MKCu
            // 
            this.lb_MKCu.AutoSize = true;
            this.lb_MKCu.BackColor = System.Drawing.Color.Transparent;
            this.lb_MKCu.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MKCu.ForeColor = System.Drawing.Color.White;
            this.lb_MKCu.Location = new System.Drawing.Point(207, 304);
            this.lb_MKCu.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb_MKCu.Name = "lb_MKCu";
            this.lb_MKCu.Size = new System.Drawing.Size(162, 33);
            this.lb_MKCu.TabIndex = 9;
            this.lb_MKCu.Text = "Mật khẩu cũ:";
            // 
            // lb_MKMoi
            // 
            this.lb_MKMoi.AutoSize = true;
            this.lb_MKMoi.BackColor = System.Drawing.Color.Transparent;
            this.lb_MKMoi.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MKMoi.ForeColor = System.Drawing.Color.White;
            this.lb_MKMoi.Location = new System.Drawing.Point(190, 382);
            this.lb_MKMoi.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb_MKMoi.Name = "lb_MKMoi";
            this.lb_MKMoi.Size = new System.Drawing.Size(179, 33);
            this.lb_MKMoi.TabIndex = 6;
            this.lb_MKMoi.Text = "Mật khẩu mới:";
            // 
            // lb_TenDangNhap
            // 
            this.lb_TenDangNhap.AutoSize = true;
            this.lb_TenDangNhap.BackColor = System.Drawing.Color.Transparent;
            this.lb_TenDangNhap.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TenDangNhap.ForeColor = System.Drawing.Color.White;
            this.lb_TenDangNhap.Location = new System.Drawing.Point(183, 217);
            this.lb_TenDangNhap.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb_TenDangNhap.Name = "lb_TenDangNhap";
            this.lb_TenDangNhap.Size = new System.Drawing.Size(186, 33);
            this.lb_TenDangNhap.TabIndex = 7;
            this.lb_TenDangNhap.Text = "Tên đăng nhập:";
            // 
            // lb_DMK
            // 
            this.lb_DMK.AutoSize = true;
            this.lb_DMK.BackColor = System.Drawing.Color.Transparent;
            this.lb_DMK.Font = new System.Drawing.Font("Times New Roman", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_DMK.ForeColor = System.Drawing.Color.White;
            this.lb_DMK.Location = new System.Drawing.Point(283, 100);
            this.lb_DMK.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb_DMK.Name = "lb_DMK";
            this.lb_DMK.Size = new System.Drawing.Size(304, 55);
            this.lb_DMK.TabIndex = 5;
            this.lb_DMK.Text = "Đổi mật khẩu";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // DoiMatKhau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.bt_Thoat);
            this.Controls.Add(this.bt_DoiMK);
            this.Controls.Add(this.txtb_XacNhanMK);
            this.Controls.Add(this.txtb_MKMoi);
            this.Controls.Add(this.txtb_MKCu);
            this.Controls.Add(this.txtb_TenDangNhap);
            this.Controls.Add(this.lb_XacNhanMK);
            this.Controls.Add(this.lb_MKCu);
            this.Controls.Add(this.lb_MKMoi);
            this.Controls.Add(this.lb_TenDangNhap);
            this.Controls.Add(this.lb_DMK);
            this.Name = "DoiMatKhau";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DoiMatKhau";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_Thoat;
        private System.Windows.Forms.Button bt_DoiMK;
        private System.Windows.Forms.TextBox txtb_XacNhanMK;
        private System.Windows.Forms.TextBox txtb_MKMoi;
        private System.Windows.Forms.TextBox txtb_MKCu;
        private System.Windows.Forms.TextBox txtb_TenDangNhap;
        private System.Windows.Forms.Label lb_XacNhanMK;
        private System.Windows.Forms.Label lb_MKCu;
        private System.Windows.Forms.Label lb_MKMoi;
        private System.Windows.Forms.Label lb_TenDangNhap;
        private System.Windows.Forms.Label lb_DMK;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}