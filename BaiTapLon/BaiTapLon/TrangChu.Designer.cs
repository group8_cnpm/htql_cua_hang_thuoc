﻿
namespace BaiTapLon
{
    partial class TrangChu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrangChu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuNhanVien = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSanPham = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuThongKe = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêLươngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêTàiChínhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêĐơnHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.picTrangChu = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTrangChu)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNhanVien,
            this.mnuSanPham,
            this.mnuThongKe});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1006, 41);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuNhanVien
            // 
            this.mnuNhanVien.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuNhanVien.Name = "mnuNhanVien";
            this.mnuNhanVien.Size = new System.Drawing.Size(142, 37);
            this.mnuNhanVien.Text = "Nhân viên";
            // 
            // mnuSanPham
            // 
            this.mnuSanPham.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuSanPham.Name = "mnuSanPham";
            this.mnuSanPham.Size = new System.Drawing.Size(138, 37);
            this.mnuSanPham.Text = "Sản phẩm";
            this.mnuSanPham.Click += new System.EventHandler(this.mnuSanPham_Click);
            // 
            // mnuThongKe
            // 
            this.mnuThongKe.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thốngKêLươngToolStripMenuItem,
            this.thốngKêTàiChínhToolStripMenuItem,
            this.thốngKêĐơnHàngToolStripMenuItem});
            this.mnuThongKe.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuThongKe.Name = "mnuThongKe";
            this.mnuThongKe.Size = new System.Drawing.Size(133, 37);
            this.mnuThongKe.Text = "Thống kê";
            // 
            // thốngKêLươngToolStripMenuItem
            // 
            this.thốngKêLươngToolStripMenuItem.Name = "thốngKêLươngToolStripMenuItem";
            this.thốngKêLươngToolStripMenuItem.Size = new System.Drawing.Size(319, 38);
            this.thốngKêLươngToolStripMenuItem.Text = "Thống kê lương";
            // 
            // thốngKêTàiChínhToolStripMenuItem
            // 
            this.thốngKêTàiChínhToolStripMenuItem.Name = "thốngKêTàiChínhToolStripMenuItem";
            this.thốngKêTàiChínhToolStripMenuItem.Size = new System.Drawing.Size(319, 38);
            this.thốngKêTàiChínhToolStripMenuItem.Text = "Thống kê tài chính ";
            // 
            // thốngKêĐơnHàngToolStripMenuItem
            // 
            this.thốngKêĐơnHàngToolStripMenuItem.Name = "thốngKêĐơnHàngToolStripMenuItem";
            this.thốngKêĐơnHàngToolStripMenuItem.Size = new System.Drawing.Size(319, 38);
            this.thốngKêĐơnHàngToolStripMenuItem.Text = "Thống kê đơn hàng";
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangXuat.Location = new System.Drawing.Point(844, -1);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(162, 42);
            this.btnDangXuat.TabIndex = 1;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.UseVisualStyleBackColor = true;
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
            // 
            // picTrangChu
            // 
            this.picTrangChu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picTrangChu.BackgroundImage")));
            this.picTrangChu.Location = new System.Drawing.Point(12, 63);
            this.picTrangChu.Name = "picTrangChu";
            this.picTrangChu.Size = new System.Drawing.Size(982, 471);
            this.picTrangChu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picTrangChu.TabIndex = 2;
            this.picTrangChu.TabStop = false;
            this.picTrangChu.Click += new System.EventHandler(this.picTrangChu_Click);
            // 
            // TrangChu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.picTrangChu);
            this.Controls.Add(this.btnDangXuat);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TrangChu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrangChu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTrangChu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuNhanVien;
        private System.Windows.Forms.ToolStripMenuItem mnuSanPham;
        private System.Windows.Forms.ToolStripMenuItem mnuThongKe;
        private System.Windows.Forms.ToolStripMenuItem thốngKêLươngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêTàiChínhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêĐơnHàngToolStripMenuItem;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.PictureBox picTrangChu;
    }
}