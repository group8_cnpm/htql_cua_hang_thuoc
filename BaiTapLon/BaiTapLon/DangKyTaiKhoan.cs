﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class DangKyTaiKhoan : Form
    {
        public DangKyTaiKhoan()
        {
            InitializeComponent();
        }

        private void DangKyTaiKhoan_Load(object sender, EventArgs e)
        {
            this.txtTenTaiKhoan.Text = "Người dùng";
        }

        private void btDangKy_Click(object sender, EventArgs e)
        {
            if (this.txtHoVaTen.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập thông tin Họ và tên", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (this.txtSoDienThoai.Text == String.Empty)
            {
                MessageBox.Show("Bạn cần nhập số điện thoại", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else if (txtMatKhau.Text != txtNhapLaiMatKhau.Text)
            {
                MessageBox.Show("Mật khẩu không trùng khớp", "Lỗi", MessageBoxButtons.OKCancel);
            }
            else
            {
                SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

                cnn.Open();
                string checkSDT = "select count(*) from nguoidung where sdt = '"+txtSoDienThoai.Text+"'";
                SqlCommand cmd = new SqlCommand(checkSDT, cnn);
                int count = (int)cmd.ExecuteScalar();
                if (count != 0)
                {
                    MessageBox.Show("Số điện thoại đã tồn tại", "Lỗi", MessageBoxButtons.OKCancel);
                }
                else {
                    string sql = "insert into NguoiDung(hoten,sdt,tentaikhoan,matkhau) values(@hovaten,@sdt,@tentaikhoan,@matkhau)";
                    cmd = new SqlCommand(sql, cnn);
                    cmd.Parameters.AddWithValue("@hovaten", this.txtHoVaTen.Text);
                    cmd.Parameters.AddWithValue("@sdt", this.txtSoDienThoai.Text);
                    cmd.Parameters.AddWithValue("@tentaikhoan", this.txtTenTaiKhoan.Text);
                    cmd.Parameters.AddWithValue("@matkhau", this.txtMatKhau.Text);
                    cmd.ExecuteNonQuery();

                    string sql1 = "SELECT MAX(MSND) FROM NGUOIDUNG";
                    SqlCommand cmd1 = new SqlCommand(sql1, cnn);

                    string n = (string)cmd1.ExecuteScalar();
                    cnn.Close();

                    string re = "Thêm người dùng với mã số: " + n.ToString().Trim() + " thành công";

                    MessageBox.Show(re, "Thông báo", MessageBoxButtons.OKCancel);
                    cnn.Close();
                    this.Hide();
                    DangNhap dN = new DangNhap();
                    dN.ShowDialog(this);
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            DangNhap dN = new DangNhap();
            dN.ShowDialog(this);
        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            DangNhap dN = new DangNhap();
            dN.ShowDialog(this);
        }
    }
}
