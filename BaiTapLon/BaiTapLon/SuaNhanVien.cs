﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    public partial class SuaNhanVien : Form
    {
        public SuaNhanVien()
        {
            InitializeComponent();
        }

        SqlConnection cnn = new SqlConnection(SqlConnect.ConnectionString);

        private void SuaNhanVien_Load(object sender, EventArgs e)
        {
            string ms = Program.getMaSoNV();
            cnn.Open();
            string sql1 = "select hoten from NguoiDung where msnd LIKE '" + ms + "'";
            SqlCommand cmd1 = new SqlCommand(sql1, cnn);
            this.txtHoTen.Text = (string)cmd1.ExecuteScalar();

            string sql2 = "select sdt from NguoiDung where msnd LIKE '" + ms + "'";
            SqlCommand cmd2 = new SqlCommand(sql2, cnn);
            this.txtSDT.Text = (string)cmd2.ExecuteScalar();

            string sql3 = "select diachi from NguoiDung where msnd LIKE '" + ms + "'";
            SqlCommand cmd3 = new SqlCommand(sql3, cnn);
            this.txtDiaChi.Text = (string)cmd3.ExecuteScalar();

            string sql4 = "select luong from NguoiDung where msnd LIKE '" + ms + "'";
            SqlCommand cmd4 = new SqlCommand(sql4, cnn);
            this.txtLuong.Text =Convert.ToString((int)cmd4.ExecuteScalar());

            string sql5 = "select tentaikhoan from NguoiDung where msnd LIKE '" + ms + "'";
            SqlCommand cmd5 = new SqlCommand(sql5, cnn);
            this.txtTenTK.Text = (string)cmd5.ExecuteScalar();

            string sql6 = "select matkhau from NguoiDung where msnd LIKE '" + ms + "'";
            SqlCommand cmd6 = new SqlCommand(sql6, cnn);
            this.txtMatKhau.Text = (string)cmd6.ExecuteScalar();

            cnn.Close();

        }

        private void btnCapNhap_Click(object sender, EventArgs e)
        {
            string ms = Program.getMaSoNV();
            cnn.Open();
            string sql = "Update NguoiDung set hoten = @hoten, sdt = @sdt, luong = @luong, diachi = @diachi, tentaikhoan = @tenTk, matkhau = @mk where msnd LIKE '"+ms+"'";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.Parameters.AddWithValue("@hoten", this.txtHoTen.Text);
            cmd.Parameters.AddWithValue("@sdt", this.txtSDT.Text);
            cmd.Parameters.AddWithValue("@luong", this.txtLuong.Text);
            cmd.Parameters.AddWithValue("@diachi", this.txtDiaChi.Text);
            cmd.Parameters.AddWithValue("@tenTK", this.txtTenTK.Text);
            cmd.Parameters.AddWithValue("@mk", this.txtMatKhau.Text);
            cmd.ExecuteNonQuery();
            cnn.Close();

            this.Hide();
            Program.getTrangChuAdmin();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.getTrangChuAdmin();
        }
    }
}
