﻿
namespace BaiTapLon
{
    partial class SuaNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SuaNhanVien));
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.lbDiaChi = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnCapNhap = new System.Windows.Forms.Button();
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.txtTenTK = new System.Windows.Forms.TextBox();
            this.txtLuong = new System.Windows.Forms.TextBox();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.lblMatKhau = new System.Windows.Forms.Label();
            this.lblTenTK = new System.Windows.Forms.Label();
            this.lblLuong = new System.Windows.Forms.Label();
            this.lblSDT = new System.Windows.Forms.Label();
            this.lblHoTen = new System.Windows.Forms.Label();
            this.lblThemNhanVien = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtDiaChi
            // 
            resources.ApplyResources(this.txtDiaChi, "txtDiaChi");
            this.txtDiaChi.Name = "txtDiaChi";
            // 
            // lbDiaChi
            // 
            resources.ApplyResources(this.lbDiaChi, "lbDiaChi");
            this.lbDiaChi.BackColor = System.Drawing.Color.Transparent;
            this.lbDiaChi.ForeColor = System.Drawing.Color.White;
            this.lbDiaChi.Name = "lbDiaChi";
            // 
            // btnThoat
            // 
            resources.ApplyResources(this.btnThoat, "btnThoat");
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnCapNhap
            // 
            resources.ApplyResources(this.btnCapNhap, "btnCapNhap");
            this.btnCapNhap.Name = "btnCapNhap";
            this.btnCapNhap.UseVisualStyleBackColor = true;
            this.btnCapNhap.Click += new System.EventHandler(this.btnCapNhap_Click);
            // 
            // txtMatKhau
            // 
            resources.ApplyResources(this.txtMatKhau, "txtMatKhau");
            this.txtMatKhau.Name = "txtMatKhau";
            // 
            // txtTenTK
            // 
            resources.ApplyResources(this.txtTenTK, "txtTenTK");
            this.txtTenTK.Name = "txtTenTK";
            // 
            // txtLuong
            // 
            resources.ApplyResources(this.txtLuong, "txtLuong");
            this.txtLuong.Name = "txtLuong";
            // 
            // txtSDT
            // 
            resources.ApplyResources(this.txtSDT, "txtSDT");
            this.txtSDT.Name = "txtSDT";
            // 
            // txtHoTen
            // 
            resources.ApplyResources(this.txtHoTen, "txtHoTen");
            this.txtHoTen.Name = "txtHoTen";
            // 
            // lblMatKhau
            // 
            resources.ApplyResources(this.lblMatKhau, "lblMatKhau");
            this.lblMatKhau.BackColor = System.Drawing.Color.Transparent;
            this.lblMatKhau.ForeColor = System.Drawing.Color.White;
            this.lblMatKhau.Name = "lblMatKhau";
            // 
            // lblTenTK
            // 
            resources.ApplyResources(this.lblTenTK, "lblTenTK");
            this.lblTenTK.BackColor = System.Drawing.Color.Transparent;
            this.lblTenTK.ForeColor = System.Drawing.Color.White;
            this.lblTenTK.Name = "lblTenTK";
            // 
            // lblLuong
            // 
            resources.ApplyResources(this.lblLuong, "lblLuong");
            this.lblLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblLuong.ForeColor = System.Drawing.Color.White;
            this.lblLuong.Name = "lblLuong";
            // 
            // lblSDT
            // 
            resources.ApplyResources(this.lblSDT, "lblSDT");
            this.lblSDT.BackColor = System.Drawing.Color.Transparent;
            this.lblSDT.ForeColor = System.Drawing.Color.White;
            this.lblSDT.Name = "lblSDT";
            // 
            // lblHoTen
            // 
            resources.ApplyResources(this.lblHoTen, "lblHoTen");
            this.lblHoTen.BackColor = System.Drawing.Color.Transparent;
            this.lblHoTen.ForeColor = System.Drawing.Color.White;
            this.lblHoTen.Name = "lblHoTen";
            // 
            // lblThemNhanVien
            // 
            resources.ApplyResources(this.lblThemNhanVien, "lblThemNhanVien");
            this.lblThemNhanVien.BackColor = System.Drawing.Color.Transparent;
            this.lblThemNhanVien.ForeColor = System.Drawing.Color.White;
            this.lblThemNhanVien.Name = "lblThemNhanVien";
            // 
            // SuaNhanVien
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtDiaChi);
            this.Controls.Add(this.lbDiaChi);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnCapNhap);
            this.Controls.Add(this.txtMatKhau);
            this.Controls.Add(this.txtTenTK);
            this.Controls.Add(this.txtLuong);
            this.Controls.Add(this.txtSDT);
            this.Controls.Add(this.txtHoTen);
            this.Controls.Add(this.lblMatKhau);
            this.Controls.Add(this.lblTenTK);
            this.Controls.Add(this.lblLuong);
            this.Controls.Add(this.lblSDT);
            this.Controls.Add(this.lblHoTen);
            this.Controls.Add(this.lblThemNhanVien);
            this.Name = "SuaNhanVien";
            this.Load += new System.EventHandler(this.SuaNhanVien_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label lbDiaChi;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnCapNhap;
        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.TextBox txtTenTK;
        private System.Windows.Forms.TextBox txtLuong;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.Label lblMatKhau;
        private System.Windows.Forms.Label lblTenTK;
        private System.Windows.Forms.Label lblLuong;
        private System.Windows.Forms.Label lblSDT;
        private System.Windows.Forms.Label lblHoTen;
        private System.Windows.Forms.Label lblThemNhanVien;
    }
}