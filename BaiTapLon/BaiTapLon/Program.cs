﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaiTapLon
{
    static class Program
    {
        static DangNhap dN = new DangNhap();
        static DangKyTaiKhoan dK = new DangKyTaiKhoan();
        static SanPham sp = new SanPham();
        static TrangChuAdmin tc = new TrangChuAdmin();
        static ThemSanPham tsp = new ThemSanPham();
        static ThemNhanVien tnv = new ThemNhanVien();
        static DoiMatKhau dmk = new DoiMatKhau();
        static string tenDN;
        static SuaNhanVien snv = new SuaNhanVien();
        static SuaSanPham ssp = new SuaSanPham();
        static TrangChuKHTV tckhtv;
        static GioHang gh = new GioHang();
        static ThanhToan tt = new ThanhToan();
        public static void getThanhToan()
        {
            tt.ShowDialog();
        }


        public static string getSdt()
        {
            return dN.getSdt();
        }

        public static void getGioHang()
        {
            gh.ketnoi();
            gh.Show();
        }
        public static void getTrangChuKHTV()
        {
            tckhtv = new TrangChuKHTV();
            tckhtv.Show();
        }
        public static void getDoiMatKhau()
        {
            dmk.Show();
        }
        public static void getThemNhanVien()
        {
            tnv.Show();
        }
        public static void getSanPham()
        {
            sp.ketnoi();
            sp.Show();
        }
        public static void getDangNhap()
        {
            dN.Show();
        }
        public static void getDangKy()
        {
            dK.Show();
        }
        public static void getTrangChuAdmin(String ten)
        {
            tc.setTenDangNhap(ten);
            tenDN = ten;
            tc.Show();
        }

        public static string getMS()
        {
            return dN.getMS();
        }
        public static void getTrangChuAdmin()
        {
            tc.ketnoi();
            tc.Show();
        }
        public static void setNull()
        {
            dN.setNull();
        }

        public static void getThemSanPham()
        {
            tsp.Show();
        }

        public static string getTenDN()
        {
            return tenDN;
        }
        public static void getThongTinSanPham()
        {
            ssp.ShowDialog();
        }

        public static string getMSSP()
        {
            return sp.getMaSoSP();
        }
        public static bool checkNum(string n)
        {
            return tsp.checkNumber(n);
        }
        public static void getThongTinNhanVien()
        {
            snv.ShowDialog();
        }

        public static string getMaSoNV()
        {
            return tc.getMSNhanVien();
        }

        public static string getTenKH()
        {
            return dN.getTenKH();
        }

        public static int getTongTien()
        {
            return gh.tongTien();
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();



            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new DangNhap());

            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(dN);


        }
    }
}